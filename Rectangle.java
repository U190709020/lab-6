import java.awt.geom.Area;
import java.security.PublicKey;

public class Rectangle {
    int sideA ;
    int sideB ;
    Point topleft;

    public Rectangle (int sideA, int sideB, Point topleft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topleft = topleft;
    }

    public int area(){
        return this.sideA * this.sideB;
    }

    public int perimeter(){
        return (this.sideA * 2) + (this.sideB * 2);
    }

    public void corners(){
        String topLeft = topleft.xCoord + "," + topleft.yCoord;
        String topRight = (topleft.xCoord + sideB ) + "," + (topleft.yCoord) ;
        String botLeft = topleft.xCoord + "," + (topleft.yCoord - sideA);;
        String botRight = (topleft.xCoord - sideB) + "," + (topleft.yCoord - sideA);
        System.out.println("Corners of rectangle: " + "(" + topLeft + ")" + " (" + topRight + ")" + " (" + botLeft + ")" + " (" + botRight + ")");
    }



}
