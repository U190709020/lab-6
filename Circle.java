public class Circle {
    int radius;
    Point center;

    public double area(){
        return radius * radius * Math.PI ;
    }

    public double perimeter() {
        return 2 * Math.PI * radius ;
    }

    public Circle(int radius , Point center){
        this.radius = radius;
        this.center = center;
    }

    public boolean intersect(Circle x , Circle y){
        double betweentwocircle = Math.sqrt(Math.pow(x.center.xCoord - y.center.xCoord , 2) + Math.pow(x.center.yCoord - y.center.yCoord , 2));
        return betweentwocircle < x.radius + y.radius;

    }
}
