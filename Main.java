public class Main {
    public static void main(String[] args) {
        Rectangle Rectangle = new Rectangle(10,20,new Point(12,4));

        System.out.println("The area of rectangle: " + Rectangle.area());
        System.out.println("The perimeter of rectangle: " + Rectangle.perimeter());
        Rectangle.corners();

        System.out.println();

        Circle Circle_1 = new Circle(10,new Point(5,5));
        Circle Circle_2 = new Circle(5,new Point(15,15));

        System.out.println("the area of circle: "+ Circle_1.area());
        System.out.println("the perimeter of circle: "+Circle_1.perimeter());
        System.out.println("Intersect of two circle: " + Circle_1.intersect(Circle_1,Circle_2));

    }
}
